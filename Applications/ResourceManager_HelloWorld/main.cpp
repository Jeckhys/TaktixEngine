#include <SFML/Graphics.hpp>

#include <spdlog/include/spdlog/spdlog.h>

#include <Engine/ResourceManager.hpp>

namespace Textures
{
    enum ID
    {
        Shit
    };
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(256, 256), "ResourceManager");

    tx::ResourceManager<sf::Texture, Textures::ID> resourceManager;

    try
    {
        resourceManager.load(Textures::Shit, "Assets/Picture.png");
    }
    catch(std::runtime_error& error)
    {
        spdlog::get("console")->critical("Exception : {0}", error.what());

        return EXIT_FAILURE;
    }

    sf::Sprite sprite(resourceManager.get(Textures::Shit));

    while (window.isOpen())
    {
        sf::Event event = {};
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        window.draw(sprite);
        window.display();
    }

    return 0;
}
