#include <iostream>

#include <ECS/ECS.h>

namespace ECS
{
    struct Position
    {
        Position(float x, float y) : x(x), y(y)
        {}

        Position() = default;

        float x;
        float y;
    };

    struct Rotation
    {
        explicit Rotation(float angle) : angle(angle)
        {}

        Rotation() = default;

        float angle;
    };

    struct SomeComponent
    {
        SomeComponent() = default;
    };

    struct SomeEvent
    {
        int num;
    };

    class TestSystem : public EntitySystem,
                       public EventSubscriber<Events::OnEntityCreated>,
                       public EventSubscriber<Events::OnEntityDestroyed>,
                       public EventSubscriber<Events::OnComponentRemoved<Position>>,
                       public EventSubscriber<Events::OnComponentRemoved<Rotation>>,
                       public EventSubscriber<SomeEvent>
    {
    public:
        ~TestSystem() override = default;

        void configure(World *world) override
        {
            world->subscribe<Events::OnEntityCreated>(this);
            world->subscribe<Events::OnEntityDestroyed>(this);
            world->subscribe<Events::OnComponentRemoved<Position>>(this);
            world->subscribe<Events::OnComponentRemoved<Rotation>>(this);
            world->subscribe<SomeEvent>(this);
        }

        void unconfigure(World *world) override
        {
            world->unsubscribeAll(this);
        }

        void tick(World *world, float deltaTime) override
        {
            world->each<Position, Rotation>(
                    [&](Entity *ent, ComponentHandle<Position> pos, ComponentHandle<Rotation> rot) -> void {
                        pos->x += deltaTime;
                        pos->y += deltaTime;
                        rot->angle += deltaTime * 2;
                    });
        }

        void receive(World *world, const Events::OnEntityCreated &event) override
        {
            std::cout << "An entity was created!" << std::endl;
        }

        void receive(World *world, const Events::OnEntityDestroyed &event) override
        {
            std::cout << "An entity was destroyed!" << std::endl;
        }

        void receive(World *world, const Events::OnComponentRemoved<Position> &event) override
        {
            std::cout << "A position component was removed!" << std::endl;
        }

        void receive(World *world, const Events::OnComponentRemoved<Rotation> &event) override
        {
            std::cout << "A rotation component was removed!" << std::endl;
        }

        void receive(World *world, const SomeEvent &event) override
        {
            std::cout << "I received SomeEvent with value " << event.num << "!" << std::endl;

            // Let's delete an entity while iterating because why not?
            world->all([&](Entity *ent) {
                if (ent->getEntityId() + 1 == event.num)
                {
                    world->destroy(world->getById(event.num));
                }

                if (ent->getEntityId() == event.num)
                {
                    std::cout << "Woah, we shouldn't get here!" << std::endl;
                }
            });
        }
    };
}

int main(int argc, char *argv[])
{
    std::cout << "EntityComponentSystem Test" << std::endl;
    std::cout << "==========================" << std::endl;

    ECS::World *world = ECS::World::createWorld();

    world->registerSystem(new ECS::TestSystem());

    ECS::Entity *ent = world->create();

    auto pos = ent->assign<ECS::Position>(0.f, 0.f);
    auto rot = ent->assign<ECS::Rotation>(0.f);

    std::cout << "Initial values : position(" << pos->x << ", " << pos->y << "), rotation(" << rot->angle << ")"
              << std::endl;

    world->tick(10.f);

    std::cout << "After tick(10) : position(" << pos->x << ", " << pos->y << "), rotation(" << rot->angle << ")"
              << std::endl;

    ent->remove<ECS::Position>();
    ent->remove<ECS::Rotation>();

    std::cout << "Creating more entities..." << std::endl;

    for (int i = 0; i < 10; ++i)
    {
        ent = world->create();
        ent->assign<ECS::SomeComponent>();
    }

    int count = 0;

    std::cout << "Counting entities with SomeComponent..." << std::endl;

    for (auto e : world->each<ECS::SomeComponent>())
    {
        ++count;
        std::cout << "Found entity #" << e->getEntityId() << std::endl;
    }

    std::cout << count << " entities have SomeComponent!" << std::endl;

    world->emit<ECS::SomeEvent>({4});

    std::cout << "We have " << world->getCount() << " entities right now." << std::endl;

    world->cleanup();

    std::cout << "After a cleanup, we have " << world->getCount() << " entities." << std::endl;
    std::cout << "Destroying the world..." << std::endl;

    world->destroyWorld();

    std::cout << "Press any key to exit..." << std::endl;
    std::getchar();

    return 0;
}