### INFORMATIONS ###

# Repository : https://gitlab.com/Jeckhys/TaktixEngine
# Pipelines  : https://gitlab.com/Jeckhys/TaktixEngine/pipelines

### STAGES CONFIGURATION ###

stages:
    - build

### VARIABLES CONFIGURATION ###

variables:
    GIT_STRATEGY: clone
    GIT_SUBMODULE_STRATEGY: recursive

### BUILD CONFIGURATION ###

.build_template: &build_configuration
    stage: build
    script:
        - ls
        - cd Libraries # TaktixEngine/Libraries
        - git clone --recursive https://github.com/SFML/SFML.git
        - cd SFML && mkdir build && cd build # TaktixEngine/Libraries/SFML/build
        - cmake .. -DCMAKE_BUILD_TYPE=Release
        - make -j2
        - make install
        - cd ../.. # TaktixEngine/Libraries
        - ls
        - git clone --recursive https://github.com/texus/TGUI.git
        - cd TGUI # TaktixEngine/Libraries/TGUI
        - git checkout 0.7
        - mkdir build && cd build # TaktixEngine/Libraries/TGUI/build
        - cmake .. -DCMAKE_BUILD_TYPE=Release
        - make -j2
        - make install
        - cd ../../.. # TaktixEngine
        - ls
        - mkdir Artifacts
        - mkdir Build
        - cd Build # TaktixEngine/Build
        - cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../Artifacts/
        - make all
        - export LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH
        - ./Tests/TaktixEngine_Tests
    artifacts:
        when: on_success
        expire_in: 2h
        paths:
            - Artifacts/*

### DEBIAN 8 (JESSIE) CONFIGURATION ###

build:debian-8:
    <<: *build_configuration
    image: debian:8
    before_script:
        - apt-get update -y
        - apt-get -y install build-essential
        - apt-get -y install git
        - apt-get -y install cmake
        - apt-get -y install doxygen
        - apt-get -y install libopenal-dev
        - apt-get -y install libjpeg-dev
        - apt-get -y install libudev-dev
        - apt-get -y install libxrandr-dev
        - apt-get -y install libfreetype6-dev
        - apt-get -y install libvorbis-dev
        - apt-get -y install libflac-dev
        - apt-get -y install libgl1-mesa-dev

### DEBIAN 9 (STRETCH) CONFIGURATION ###

build:debian-9:
    <<: *build_configuration
    image: debian:9
    before_script:
        - apt-get update -y
        - apt-get -y install build-essential
        - apt-get -y install git
        - apt-get -y install cmake
        - apt-get -y install doxygen
        - apt-get -y install libopenal-dev
        - apt-get -y install libjpeg-dev
        - apt-get -y install libudev-dev
        - apt-get -y install libxrandr-dev
        - apt-get -y install libfreetype6-dev
        - apt-get -y install libvorbis-dev
        - apt-get -y install libflac-dev
        - apt-get -y install libgl1-mesa-dev

### UBUNTU 14.04 (TRUSTY) CONFIGURATION

build:ubuntu-14.04:
    <<: *build_configuration
    image: ubuntu:trusty
    before_script:
        - apt-get update -y
        - apt-get -y install software-properties-common
        - add-apt-repository -y ppa:george-edison55/cmake-3.x
        - apt-get update -y
        - apt-get -y install build-essential
        - apt-get -y install git
        - apt-get -y install cmake
        - apt-get -y install doxygen
        - apt-get -y install libopenal-dev
        - apt-get -y install libjpeg-dev
        - apt-get -y install libudev-dev
        - apt-get -y install libxrandr-dev
        - apt-get -y install libfreetype6-dev
        - apt-get -y install libvorbis-dev
        - apt-get -y install libflac-dev
        - apt-get -y install libgl1-mesa-dev

### UBUNTU 16.04 (XENIAL) CONFIGURATION

build:ubuntu-16.04:
    <<: *build_configuration
    image: ubuntu:xenial
    before_script:
        - apt-get update -y
        - apt-get -y install build-essential
        - apt-get -y install git
        - apt-get -y install cmake
        - apt-get -y install doxygen
        - apt-get -y install libopenal-dev
        - apt-get -y install libjpeg-dev
        - apt-get -y install libudev-dev
        - apt-get -y install libxrandr-dev
        - apt-get -y install libfreetype6-dev
        - apt-get -y install libvorbis-dev
        - apt-get -y install libflac-dev
        - apt-get -y install libgl1-mesa-dev

### UBUNTU 18.04 (BIONIC) CONFIGURATION

build:ubuntu-18.04:
    <<: *build_configuration
    image: ubuntu:bionic
    before_script:
        - apt-get update -y
        - apt-get -y install build-essential
        - apt-get -y install git
        - apt-get -y install cmake
        - apt-get -y install doxygen
        - apt-get -y install libopenal-dev
        - apt-get -y install libjpeg-dev
        - apt-get -y install libudev-dev
        - apt-get -y install libxrandr-dev
        - apt-get -y install libfreetype6-dev
        - apt-get -y install libvorbis-dev
        - apt-get -y install libflac-dev
        - apt-get -y install libgl1-mesa-dev

### FEDORA 24 CONFIGURATION

build:fedora-24:
    <<: *build_configuration
    image: fedora:24
    before_script:
        - dnf -y install make
        - dnf -y install automake
        - dnf -y install gcc
        - dnf -y install gcc-c++
        - dnf -y install kernel-devel
        - dnf -y install git
        - dnf -y install cmake
        - dnf -y install doxygen
        - dnf -y install openal*
        - dnf -y install libjpeg*
        - dnf -y install systemd-devel # contains Fedora's libudev-dev
        - dnf -y install libXrandr-devel
        - dnf -y install freetype-devel
        - dnf -y install libvorbis*
        - dnf -y install flac-devel
        - dnf -y install mesa-libGL-devel

### FEDORA 25 CONFIGURATION

build:fedora-25:
    <<: *build_configuration
    image: fedora:25
    before_script:
        - dnf -y install make
        - dnf -y install automake
        - dnf -y install gcc
        - dnf -y install gcc-c++
        - dnf -y install kernel-devel
        - dnf -y install git
        - dnf -y install cmake
        - dnf -y install doxygen
        - dnf -y install openal*
        - dnf -y install libjpeg*
        - dnf -y install systemd-devel # contains Fedora's libudev-dev
        - dnf -y install libXrandr-devel
        - dnf -y install freetype-devel
        - dnf -y install libvorbis*
        - dnf -y install flac-devel
        - dnf -y install mesa-libGL-devel

### FEDORA 26 CONFIGURATION

build:fedora-26:
    <<: *build_configuration
    image: fedora:26
    before_script:
        - dnf -y install make
        - dnf -y install automake
        - dnf -y install gcc
        - dnf -y install gcc-c++
        - dnf -y install kernel-devel
        - dnf -y install git
        - dnf -y install cmake
        - dnf -y install doxygen
        - dnf -y install openal*
        - dnf -y install libjpeg*
        - dnf -y install systemd-devel # contains Fedora's libudev-dev
        - dnf -y install libXrandr-devel
        - dnf -y install freetype-devel
        - dnf -y install libvorbis*
        - dnf -y install flac-devel
        - dnf -y install mesa-libGL-devel

### FEDORA 27 CONFIGURATION

build:fedora-27:
    <<: *build_configuration
    image: fedora:27
    before_script:
        - dnf -y install make
        - dnf -y install automake
        - dnf -y install gcc
        - dnf -y install gcc-c++
        - dnf -y install kernel-devel
        - dnf -y install git
        - dnf -y install cmake
        - dnf -y install doxygen
        - dnf -y install openal*
        - dnf -y install libjpeg*
        - dnf -y install systemd-devel # contains Fedora's libudev-dev
        - dnf -y install libXrandr-devel
        - dnf -y install freetype-devel
        - dnf -y install libvorbis*
        - dnf -y install flac-devel
        - dnf -y install mesa-libGL-devel

### FEDORA 28 CONFIGURATION

build:fedora-28:
    <<: *build_configuration
    image: fedora:28
    before_script:
        - dnf -y install make
        - dnf -y install automake
        - dnf -y install gcc
        - dnf -y install gcc-c++
        - dnf -y install kernel-devel
        - dnf -y install git
        - dnf -y install cmake
        - dnf -y install doxygen
        - dnf -y install openal*
        - dnf -y install libjpeg*
        - dnf -y install systemd-devel # contains Fedora's libudev-dev
        - dnf -y install libXrandr-devel
        - dnf -y install freetype-devel
        - dnf -y install libvorbis*
        - dnf -y install flac-devel
        - dnf -y install mesa-libGL-devel
