#ifndef TAKTIXENGINE_TAKTIXENGINE_HPP
#define TAKTIXENGINE_TAKTIXENGINE_HPP

#include <SFML/Graphics/RenderWindow.hpp>

#include <spdlog/spdlog.h>

#include <Core/StateManager.hpp>

namespace tx
{
    class TaktixEngine
    {
    public:
        explicit TaktixEngine(sf::Uint16 width, sf::Uint16 height, sf::String title);
        ~TaktixEngine() = default;

        template<typename T>
        void run();

    private:
        std::shared_ptr<spdlog::logger> m_loggerConsole;
        sf::RenderWindow m_renderWindow;
        StateManager m_stateManager;
    };
}

#include <TaktixEngine.inl>

#endif //TAKTIXENGINE_TAKTIXENGINE_HPP
