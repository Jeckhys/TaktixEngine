#ifndef TAKTIXENGINE_RESOURCEMANAGER_INL
#define TAKTIXENGINE_RESOURCEMANAGER_INL

namespace tx
{
    template <typename Resource, typename Identifier>
    void ResourceManager<Resource, Identifier>::load(Identifier id, const std::string& filename)
    {
        std::unique_ptr<Resource> resource(new Resource());

        if (!resource->loadFromFile(filename))
        {
            throw std::runtime_error("ResourceHolder::load - Failed to load " + filename);
        }

        loadInternal(id, std::move(resource));
    }

    template <typename Resource, typename Identifier>
    template <typename Parameter>
    void ResourceManager<Resource, Identifier>::load(
            Identifier id, const std::string& filename, const Parameter& parameter
    ) {
        std::unique_ptr<Resource> resource(new Resource());

        if (!resource->loadFromFile(filename, parameter))
        {
            throw std::runtime_error("ResourceHolder::load - Failed to load " + filename);
        }

        loadInternal(id, std::move(resource));
    }

    template <typename Resource, typename Identifier>
    Resource& ResourceManager<Resource, Identifier>::get(Identifier id)
    {
        auto found = m_resources.find(id);
        assert(found != m_resources.end());

        return *found->second;
    }

    template <typename Resource, typename Identifier>
    const Resource& ResourceManager<Resource, Identifier>::get(Identifier id) const
    {
        auto found = m_resources.find(id);
        assert(found != m_resources.end());

        return *found->second;
    }

    template <typename Resource, typename Identifier>
    void ResourceManager<Resource, Identifier>::loadInternal(Identifier id, std::unique_ptr<Resource> resource)
    {
        auto inserted = m_resources.insert(std::make_pair(id, std::move(resource)));
        assert(inserted.second);
    }
}

#endif //TAKTIXENGINE_RESOURCEMANAGER_INL
