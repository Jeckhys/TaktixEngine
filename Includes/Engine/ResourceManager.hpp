#ifndef TAKTIXENGINE_RESOURCEMANAGER_HPP
#define TAKTIXENGINE_RESOURCEMANAGER_HPP

#include <map>
#include <string>
#include <memory>
#include <cassert>
#include <stdexcept>

namespace tx
{
    template<typename Resource, typename Identifier>
    class ResourceManager
    {
    public:
        void load(Identifier id, const std::string& filename);

        template<typename Parameter>
        void load(Identifier id, const std::string& filename, const Parameter& parameter);

        Resource& get(Identifier id);
        const Resource& get(Identifier id) const;

    private:
        void loadInternal(Identifier id, std::unique_ptr<Resource> resource);

    private:
        std::map<Identifier, std::unique_ptr<Resource>> m_resources;
    };
}

#include <Engine/ResourceManager.inl>

#endif //TAKTIXENGINE_RESOURCEMANAGER_HPP
