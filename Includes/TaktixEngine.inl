#ifndef TAKTIXENGINE_TAKTIXENGINE_INL
#define TAKTIXENGINE_TAKTIXENGINE_INL

namespace tx
{
    template<typename T>
    void TaktixEngine::run()
    {
        m_stateManager.push<T>();

        while(m_renderWindow.isOpen() && !m_stateManager.empty())
        {
            m_stateManager.handleEvent();
            m_stateManager.handleUpdate();
            m_stateManager.handleDisplay();
        }
    }
}

#endif //TAKTIXENGINE_TAKTIXENGINE_INL
